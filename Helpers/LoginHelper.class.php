<?php

namespace User\Helpers;

use Core\Db2\Models\SearchQuery;
use User\Models\Token;
use User\Models\User;

class LoginHelper{

    public function createAuthSession($userId, $searchType = SEARCH_BY_NAME){

        $user = new User($userId, $searchType);

        $sessionToken = new Token();
        $sessionToken->generateToken(-1, 900, 'USER_AUTH', $user->getId());

        return array(
            'session' => $sessionToken->getName(),
            'user' => $user->pack(array('Metadata', 'Access'))
        );

    }

    public function findByCredential($authService, $credential){
        $search = new SearchQuery(new User());
        $search->filter(['Credentials.' . $authService, '=', $credential]);
        $search->limit(1);
        $user = $search->run();

        if(count($user) > 0){
            return $user[0];
        }else{
            return false;
        }
    }

    public function provisionUser($data, $map, $credentials, $access = array()){
        $user = new User();

        $user->setCredentials($credentials);

        $hasName = false;
        foreach($map as $target => $item){
            if($target === 'name'){
                $user->setName($data[$item]);
                $hasName = true;
            }else{
                $user->setMetadata($target, $data[$item]);
            }
        }

        if(!$hasName){
            throw new \Exception("User must have name", 500);
        }

        if(is_string($access)){
            $access = array($access);
        }

        if(is_array($access) && count($access) > 0){
            $accessList = array();
            foreach($access as $group){
                $accessList[$group] = -1;
            }
            $user->setAccess($accessList);
        }

        $user->persist();

        return $user->getName();
    }

}
