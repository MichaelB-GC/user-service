<?php

namespace User\Controllers;

use Core\Controllers\BaseController;
use Core\Routing\Request;
use Core\Routing\Response;
use User\Models\User;

/**  
 * Class for handling requests to /user
 * Built by the Indigo Storm developer tool
 * @package User\Controllers
 */  
class UserController extends BaseController{

    /**  
     * @param $request  Request  The request object from Slim
     * @param $response Response  The Slim response object
     * @param $args     array   Array of arguments available from the request
     */  
    public function handleGet(Request $request, Response $response, array $args){

        if(array_key_exists('username', $args)){
            $user = new User($args['username']);
        }else{
            $user = new User($request->getUser(), SEARCH_BY_ID);
        }

        return $user->pack(array('Metadata', 'Access'));

    }

}
