<?php

namespace User\Controllers;

use Core\Controllers\BaseController;
use Core\Db2\Models\SearchQuery;
use Core\Routing\Request;
use Core\Routing\Response;
use User\Helpers\LoginHelper;
use User\Models\Token;
use User\Models\User;

/**
 * Class for handling requests to /auth
 * Built by the Indigo Storm developer tool
 * @package User\Controllers
 */
class AuthController extends BaseController{

    /**
     * @param $request  object  The request object from Slim
     * @param $response object  The Slim response object
     * @param $args     array   Array of arguments available from the request
     */
    public function handleGet($request, $response, $args){
        global $Application;

        if(array_key_exists('tokenName', $args)){
            return $this->handlePing($args['tokenName']);
        }else{
            $session = $request->getHeader('is-session');
            if(is_array($session) && count($session) === 1){
                return $this->handlePing($session[0]);
            }else{
                throw new \Exception("Session token not found", 404);
            }
        }

    }

    /**
     * @param $request
     * @param $response
     * @param $args
     * @return bool
     * @throws \Exception
     */
    public function handleDelete($request, $response, $args){

        if($args['tokenName'] != ''){
            return $this->handleExpiry($args['tokenName']);
        }else{
            return true;
        }

    }

    private function handleExpiry($tokenName){
        $token = new Token($tokenName);
        $token->expireToken();

        return true;
    }

    /**
     * @param $tokenName
     * @return array
     * @throws \Exception
     */
    private function handlePing($tokenName){
        $token = new Token($tokenName);

        if($token->useToken('USER_AUTH')){
            return array(
                'session' => $token->getName(),
            );
        }else{
            throw new \Exception("Unauthorised", 401);
        }
    }

    private function handleLogin($username, $password){

        $user = new User($username);
        if(!$user->validatePassword($password)){
            throw new \Exception('Resource not found user', 404);
        }

        $loginHelper = new LoginHelper();

        return $loginHelper->createAuthSession($user->getId(), SEARCH_BY_ID);

    }

    /**
     * @param $request  Request  The request object from Slim
     * @param $response Response  The Slim response object
     * @param $args     array   Array of arguments available from the request
     */
    public function handlePost(Request $request, Response $response, array $args){

        // Auth used to be a POST handler which has since been disassembled but this handles backwards compatibility
        // Note: SSO login is NOT included in this backwards compatibility, it MUST be updated to function correctly

        $payload = $request->getParsedBody();

        if(array_key_exists('username', $payload) && array_key_exists('password', $payload)){
            // This controller also handles /login, so check that's not in use before warning
            if(strtolower($request->getRouteName()) === '/user/auth'){
                islog(LOG_WARNING, "POSTing to /auth is deprecated and will be removed, use /login");
            }

            return $this->handleLogin($payload['username'], $payload['password']);
        }

        if(array_key_exists('authToken', $payload) && array_key_exists('action', $payload)){

            switch ($payload['action']){
                case 'logout':
                    return $this->handleExpiry($payload['authToken']);
                    break;
                case 'ping':
                    islog(LOG_WARNING, "POSTing to /auth is deprecated and will be removed, use GET");

                    return $this->handlePing($payload['authToken']);
                    break;
            }

        }

        if(array_key_exists('ssoToken', $payload)){
            islog(LOG_ERR, "User service no longer natively handles SSO, use a dedicated service");
        }

        throw new \Exception('Malformed authentication request', 500);

    }

}
