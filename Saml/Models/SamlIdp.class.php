<?php

namespace User\Saml\Models;

use Core\Models\BaseModel;

class SamlIdp extends BaseModel{

    function configure(){
        $this->addDataTable('ImportedMetadata', DB2_VARCHAR, DB2_BLOB_LONG);
        $this->addDataTable('Settings', DB2_VARCHAR_SHORT, DB2_BLOB);
    }

    public function getEntityId(){
        return $this->getImportedMetadata('entityid');
    }
}
